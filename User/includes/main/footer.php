<footer class="container text-center">
<strong>Skincarely.ph</strong> &copy; 2021. All rights reserved.
  <div class="float-right d-none d-sm-inline-block">
    <b>Version</b> 1.0
  </div>
</footer>  
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>  
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>    
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script> 
<script src="../../dist/js/popper.min.js"></script>
<script src="../../dist/js/holder.min.js"></script> 
<!-- Main App -->
<script src="dist/js/main.js"></script>   
<script>
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script> 
</body>
</html>
